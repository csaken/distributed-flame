// ID type. Each item should have a required Id used to identify the item.
typedef i64 ID

// date and time of an event since 'epoch'
typedef i64 Timestamp

// size type
typedef i32 Size

// a block pf data transfered between client & server
struct Data {
	1: required string hash;	// md5 hash of body
	2: required Size size;		// size of data
	3: required string body;	// the data itself
}

enum PrivilegeLevel {
	NORMAL,
	MANAGER,
	ADMIN
}

enum JobStatus {
	NEW,
	RENDERING,
	ASSEMBLING,
	COMPLETED,
	ERROR
}

enum TaskStatus {
	NEW,
	IN_PROGRESS,
	COMPLETED,
	ERROR
}

exception InvalidUserException {
	1: required string error;
}

// used to provide publicly available user information about a particular account
struct PublicUserInfo {
	1: required ID id;
	2: required PrivilegeLevel privilegeLevel;
	3: required string userName;
	4: required Timestamp lastActivity;
}

struct UserAttributes {
	1: optional string comments;
	2: optional string groupName;
	3: optional string address;
}

struct User {
	1: required ID id;
	2: required string username;
	3: required string email;
	4: required string name;
	5: required PrivilegeLevel privilegeLevel;
	6: required Timestamp created;
	7: required Timestamp updated;
	8: required Timestamp deleted;
	9: required bool active;
	10: required Timestamp lastActivity;
	100: optional UserAttributes attributes;
}

struct AuthenticationResult {
	1: required Timestamp timestamp;
	2: required string authToken;
	3: required Timestamp expiration;
	100: optional PublicUserInfo publicUser;
	101: optional User user;
}

struct Task {
	1: required ID id;
	2: required Timestamp created;
	3: required Timestamp updated;
	4: required Timestamp deleted;
	5: required bool active;
	6: required ID jobId;
	7: required string type;	// task type
	8: required Data data;		// task data
	9: required TaskStatus status;
	100: optional string error;	// if has errors
	101: optional map<string,string> taskOptions;
}

struct Job {
	1: required ID id;
	2: required Timestamp created;
	3: required Timestamp updated;
	4: required Timestamp deleted;
	5: required bool active;
	6: required ID owner;
	7: required string type;	// job type
	8: required Data data;		// job data
	9: required JobStatus status;
	100: optional string error;	// if has errors
	101: optional map<string,string> jobOptions;
	102: optional set<Task> taskList;
}

service DistributedFlame {
	AuthenticationResult authenticate(1:string username, 2:string password) throws (1:InvalidUserException invalidUser);
	AuthenticationResult refreshAuthentication(1:string authToken);
	PublicUserInfo getPublicUserInfo(1:string username);
	User getUser(1:string authToken);

	Job createJob(1:string authToken, 2:Job job);
	i32 deleteJob(1:string authToken, 2:ID jobId);
	set<Task> getJobTasks(1:string authToken, 2:ID jobId);
	set<Task> getReadyTasks(1:string authToken, 2:ID jobId);
	set<Task> getAvailableTasks(1:string authToken, 2:ID jobId);

	Task getNextTask(1:string authToken);
	bool UpdateTaskResult(1:ID taskId, 2:Data data)	//send back to server the result of task execution
}
