# define header and source files for the library
set (HEADER_FILES
 include/DistributedFlame_client.h
)

set (SOURCE_FILES
src/DistributedFlame_client.cpp
)

# include headers
INCLUDE_DIRECTORIES(include)

INCLUDE_DIRECTORIES( ${DF_SOURCE}/common/include)
INCLUDE_DIRECTORIES(${Thrift_INCLUDE_DIR})


# setup target
ADD_EXECUTABLE(dfsimple ${HEADER_FILES} ${SOURCE_FILES})

TARGET_LINK_LIBRARIES(dfsimple df ${Thrift_LIBRARY} sqlite3)
