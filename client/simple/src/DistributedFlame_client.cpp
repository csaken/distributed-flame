//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reproduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------

#include <transport/TSocket.h>
#include <transport/TSSLSocket.h>
#include <transport/TTransportUtils.h>
#include <protocol/TBinaryProtocol.h>

#include "DistributedFlame.h"
#include "DistributedFlame_client.h"

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

using boost::shared_ptr;

#define _SSL
#define HOST "localhost"
#define PORT 9090

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
	shared_ptr<TTransport> transport;
	shared_ptr<TProtocol> protocol;

	shared_ptr<TSocket> socket;
	shared_ptr<TSSLSocketFactory> factory;

#ifdef SSL
	factory = shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());

	factory->ciphers("ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
	factory->loadTrustedCertificates("./trusted-ca-certificate.pem");

	factory->authenticate(true);
	socket = factory->createSocket(HOST, PORT);
#else
	socket = shared_ptr<TSocket>(new TSocket(HOST, PORT));
#endif

	shared_ptr<TBufferedTransport> bufferedSocket(new TBufferedTransport(socket));
	transport = bufferedSocket;

	shared_ptr<TBinaryProtocol> binaryProtocol(new TBinaryProtocol(transport));
	protocol = binaryProtocol;

	DistributedFlameClient client(protocol);

	try
	{
		transport->open();
	}
	catch (TTransportException& ttx)
	{
		printf("Connect failed: %s\n", ttx.what());
	}

	AuthenticationResult res;
	client.authenticate(res, "jancsi",".jancsi.");

	printf("Auth token is %s\n", res.authToken.c_str());
}
