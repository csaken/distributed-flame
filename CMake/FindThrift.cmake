include(LibFindMacros)

libfind_pkg_check_modules(Thrift_PKGCONF thrift)

find_path(Thrift_INCLUDE_DIR
  NAMES Thrift.h
  PATHS ${Thrift_PKGCONF_INCLUDE_DIRS}
)

find_library(Thrift_LIBRARY
  NAMES thrift
  PATHS ${Thrift_PKGCONF_LIBRARY_DIRS}
)

set(Thrift_PROCESS_INCLUDES Thrift_INCLUDE_DIR)
set(Thrift_PROCESS_LIBS Thrift_LIBRARY)
libfind_process(Thrift)
