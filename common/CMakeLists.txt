#PROJECT(DF)

# define header and source files for the library
set (HEADER_FILES
include/df_constants.h  include/df_types.h  include/DistributedFlame.h
)

set (SOURCE_FILES
src/df_constants.cpp  src/df_types.cpp  src/DistributedFlame.cpp
src/missing.cpp
)

# include headers
INCLUDE_DIRECTORIES(include)

INCLUDE_DIRECTORIES(${Thrift_INCLUDE_DIR})

# setup target
ADD_LIBRARY(df STATIC ${HEADER_FILES} ${SOURCE_FILES})

SET_TARGET_PROPERTIES(df PROPERTIES CLEAN_DIRECT_OUTPUT 1)
