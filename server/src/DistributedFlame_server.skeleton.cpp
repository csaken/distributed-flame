// This autogenerated skeleton file illustrates how to build a server.
// You should copy it to another filename to avoid overwriting it.

#include "DistributedFlame.h"
#include <protocol/TBinaryProtocol.h>
#include <server/TSimpleServer.h>
#include <transport/TServerSocket.h>
#include <transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

class DistributedFlameHandler : virtual public DistributedFlameIf {
 public:
  DistributedFlameHandler() {
    // Your initialization goes here
  }

  void authenticate(AuthenticationResult& _return, const std::string& username, const std::string& password) {
    // Your implementation goes here
    printf("authenticate\n");
  }

  void refreshAuthentication(AuthenticationResult& _return, const std::string& authToken) {
    // Your implementation goes here
    printf("refreshAuthentication\n");
  }

  void getPublicUserInfo(PublicUserInfo& _return, const std::string& username) {
    // Your implementation goes here
    printf("getPublicUserInfo\n");
  }

  void getUser(User& _return, const std::string& authToken) {
    // Your implementation goes here
    printf("getUser\n");
  }

  void createJob(Job& _return, const std::string& authToken, const Job& job) {
    // Your implementation goes here
    printf("createJob\n");
  }

  int32_t deleteJob(const std::string& authToken, const ID jobId) {
    // Your implementation goes here
    printf("deleteJob\n");
  }

};

int main(int argc, char **argv) {
  int port = 9090;
  shared_ptr<DistributedFlameHandler> handler(new DistributedFlameHandler());
  shared_ptr<TProcessor> processor(new DistributedFlameProcessor(handler));
  shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
  return 0;
}

