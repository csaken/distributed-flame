//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reoriduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <sqlite3.h>
#include <iostream>
#include <sstream>

#include "SqliteDbManager.h"
#include <map>

using namespace std;

//-----------------------------------------------------------------------------
SqliteDbManager::SqliteDbManager()
	:db(NULL)
{
	int rc = sqlite3_open("df.db", &db);

	if( rc )
	{
		cerr<< "Can't open database:" << sqlite3_errmsg(db)<<endl;
		sqlite3_close(db);
	}

	createTablesIfNotExist();
}

//-----------------------------------------------------------------------------
SqliteDbManager::~SqliteDbManager()
{
	if (db)
	{
		sqlite3_close(db);
	}
}

static const char users_tbl_stmt[] = "CREATE TABLE users ( "
"    ID             INTEGER        PRIMARY KEY AUTOINCREMENT "
"                                  NOT NULL "
"                                  UNIQUE, "
"    username       VARCHAR( 32 )  NOT NULL, "
"    email          VARCHAR( 64 )  NOT NULL, "
"    name           VARCHAR( 64 )  NOT NULL, "
"    privilegeLevel INTEGER        NOT NULL, "
"    created        DATETIME       NOT NULL "
"                                  DEFAULT ( datetime( 'now' )  ), "
"    updated        DATETIME       NOT NULL, "
"    deleted        DATETIME       NOT NULL, "
"    active         CHAR           NOT NULL, "
"    lastActivity   DATETIME       NOT NULL  "
"	 password       VARCHAR( 255 ) NOT NULL  "
"); ";

static const char tokens_tbl_stmt[] = "CREATE TABLE tokens ( "
"    ID        INTEGER         PRIMARY KEY AUTOINCREMENT "
"                              NOT NULL "
"                              UNIQUE, "
"    userId    INTEGER         NOT NULL, "
"    authToken VARCHAR( 255 )  NOT NULL, "
"    created   DATETIME        NOT NULL  "
"); ";

static const char tasks_tbl_stmt[] = "CREATE TABLE tasks ( "
"    ID      INTEGER        PRIMARY KEY AUTOINCREMENT "
"                           NOT NULL "
"                           UNIQUE, "
"    created DATETIME       NOT NULL, "
"    updated DATETIME       NOT NULL, "
"    deleted DATETIME       NOT NULL, "
"    active  CHAR           NOT NULL, "
"    jobId   INTEGER        NOT NULL, "
"    type    VARCHAR( 32 )  NOT NULL, "
"    data    BLOB           NOT NULL, "
"    status  CHAR           NOT NULL, "
"    error   TEXT "
"); ";

static const char jobs_tbl_stmt[] = "CREATE TABLE jobs ( "
"    IDi     INTEGER        PRIMARY KEY AUTOINCREMENT "
"                           NOT NULL "
"                           UNIQUE, "
"    created DATETIME       NOT NULL, "
"    updated DATETIME       NOT NULL, "
"    deleted DATETIME, "
"    active  CHAR           NOT NULL, "
"    owner   INTEGER        NOT NULL, "
"    type    VARCHAR( 32 ), "
"    data    BLOB           NOT NULL, "
"    status  CHAR           NOT NULL, "
"    error   TEXT "
"); ";

static const char job_options_tbl_stmt[] = "CREATE TABLE job_options ( "
"    ID    INTEGER         PRIMARY KEY AUTOINCREMENT "
"                          NOT NULL "
"                          UNIQUE, "
"    jobId INTEGER         NOT NULL, "
"    name  VARCHAR( 64 )   NOT NULL, "
"    value VARCHAR( 255 )  NOT NULL  "
"); ";

static const char task_options_tbl_stmt[] = "CREATE TABLE task_options ( "
"    ID    INTEGER         PRIMARY KEY AUTOINCREMENT "
"                          NOT NULL "
"                          UNIQUE, "
"    taskId INTEGER         NOT NULL, "
"    name  VARCHAR( 64 )   NOT NULL, "
"    value VARCHAR( 255 )  NOT NULL  "
"); ";


//-----------------------------------------------------------------------------
// create the sqlite tables if they dont exist yet
//
//		User
//		-----
//		*int id autoincrement
//		string username
//		string email
//		string name
//		int privilegeLevel
//		timestamp created
//		timestamp updated
//		timestamp deleted
//		int active
//		timestamp lastActivity
//-----------------------------------------------------------------------------
void SqliteDbManager::createTablesIfNotExist()
{
	sqlite3_stmt* statement;

	// users
	if (sqlite3_prepare_v2(db, users_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table users already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}
	sqlite3_finalize(statement);

	// tokens
	if (sqlite3_prepare_v2(db, tokens_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table tokens already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}
	sqlite3_finalize(statement);

	// jobs
	if (sqlite3_prepare_v2(db, jobs_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table jobs already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}

	// tasks
	if (sqlite3_prepare_v2(db, tasks_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table tasks already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}
	sqlite3_finalize(statement);

	// job_options
	if (sqlite3_prepare_v2(db, job_options_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table job_options already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}
	sqlite3_finalize(statement);

	// tasks_options
	if (sqlite3_prepare_v2(db, task_options_tbl_stmt, -1, &statement, 0) != SQLITE_OK)
	{
		cout<<"Table task_options already exists"<<endl;
	}
	else
	{
		sqlite3_step(statement);
	}
	sqlite3_finalize(statement);
}

//-----------------------------------------------------------------------------
std::vector<std::map<std::string, std::string> > SqliteDbManager::query(std::string query)
{
	sqlite3_stmt *statement;
	vector<map<string, string> > results;

	if (sqlite3_prepare_v2(db, query.c_str(), -1, &statement, 0) == SQLITE_OK)
	{
		int cols = sqlite3_column_count(statement);
		int result = 0;

		while ( true )
		{
			result = sqlite3_step(statement);

			if(result == SQLITE_ROW)
			{
				map<string, string> values;
				for(int col = 0; col < cols; col++)
				{
					std::string  val;
					char* ptr = (char*)sqlite3_column_text(statement, col);
					if (ptr)
					{
						val = ptr;
					}
					else
					{
						// val remains empty string
					}

					values[sqlite3_column_name(statement, col)] = val;
				}

				results.push_back(values);
			}
			else
			{
				break;
			}
		}

		sqlite3_finalize(statement);

		string error = sqlite3_errmsg(db);
		if(error != "not an error") cout << query << " " << error << endl;

		return results;
	}
}

//-----------------------------------------------------------------------------
bool SqliteDbManager::isValidUser(std::string user, std::string password)
{
	sqlite3_stmt* statement;
	stringstream q;
	q<<"select ID,username from users where username='";
	q<<user;
	q<<"' and password='";
	q<<password;
	q<<"'";

	cout<<"Runnig query '"<<q.str()<<"'"<<endl;

	vector<map<string, string> > res = query(q.str());

	if (res.size() == 0)
	{
		return false;
	}

	return true;
}
