//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reproduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------

#include <iostream>
#include <sstream>

#include "DistributedFlameHandler.h"
#include "DistributedFlame.h"

#include "SqliteDbManager.h"

using namespace std;

//-----------------------------------------------------------------------------
DistributedFlameHandler::DistributedFlameHandler()
{
	SqliteDbManager::instance();
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::authenticate(AuthenticationResult& _return,
										   const std::string& username,
										   const std::string& password)
				throw (InvalidUserException, apache::thrift::TException)
{
	// Your implementation goes here
	cout<< "authenticatint user "<<username<<endl;

	stringstream q;
	q<<"select ID,username from users where username='";
	q<<username;
	q<<"' and password='";
	q<<password;
	q<<"'";

	cout<<"Runnig query '"<<q.str()<<"'"<<endl;

	vector<map<string, string> > res = SqliteDbManager::instance()->query(q.str());

	if (res.size() == 0)
	{
		cout<<"User is invalid";
		InvalidUserException ex;

		ex.error = "Invalid user";
		throw ex;
	}
	else
	{
		std::stringstream ss;
		ss << generateRandomUUID();

		_return.authToken=ss.str();
	}

}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::refreshAuthentication(AuthenticationResult& _return,
													const std::string& authToken)
{
	// Your implementation goes here
	printf("refreshAuthentication\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getPublicUserInfo(PublicUserInfo& _return,
												const std::string& username)
{
	// Your implementation goes here
	printf("getPublicUserInfo\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getUser(User& _return, const std::string& authToken)
{
	// Your implementation goes here
	printf("getUser\n");
}

void DistributedFlameHandler::createJob(Job& _return, const std::string& authToken,
										const Job& job)
{
	// Your implementation goes here
	printf("createJob\n");
}

//-----------------------------------------------------------------------------
int32_t DistributedFlameHandler::deleteJob(const std::string& authToken, const ID jobId)
{
	// Your implementation goes here
	printf("deleteJob\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getJobTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId)
{
	// Your implementation goes here
	printf("getJobTasks\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getReadyTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId)
{
	// Your implementation goes here
	printf("getJobTasks\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getAvailableTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId)
{
	// Your implementation goes here
	printf("getJobTasks\n");
}

//-----------------------------------------------------------------------------
void DistributedFlameHandler::getNextTask(Task& _return, const std::string& authToken)
{
	// Your implementation goes here
	printf("getJobTasks\n");
}

//-----------------------------------------------------------------------------
bool DistributedFlameHandler::UpdateTaskResult(const ID taskId, const Data& data)
{
	// Your implementation goes here
	printf("getJobTasks\n");
}

//-----------------------------------------------------------------------------
boost::uuids::uuid DistributedFlameHandler::generateRandomUUID()
{
	boost::uuids::random_generator gen;
	return gen();
}
