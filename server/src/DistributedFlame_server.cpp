//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reproduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------

#include <concurrency/ThreadManager.h>
#include <concurrency/PosixThreadFactory.h>
#include <protocol/TBinaryProtocol.h>
#include <server/TSimpleServer.h>
#include <transport/TServerSocket.h>
#include <transport/TBufferTransports.h>
#include <transport/TSSLServerSocket.h>
#include <transport/TSSLSocket.h>
#include <server/TThreadPoolServer.h>

#include "DistributedFlame.h"
#include "DistributedFlameHandler.h"


using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

#define MAX_WORKERS 5

#define _SSL
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
	int port = 9090;

	shared_ptr<DistributedFlameHandler> handler(new DistributedFlameHandler());
	shared_ptr<TProcessor> processor(new DistributedFlameProcessor(handler));
#ifndef SSL
	shared_ptr<TServerTransport> serverSocket(new TServerSocket(port));
#else
	// begin SSL
	shared_ptr<TSSLSocketFactory> sslSocketFactory;
	shared_ptr<TServerSocket> serverSocket;

	sslSocketFactory = shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());

	sslSocketFactory->loadCertificate("./server-certificate.pem");
	sslSocketFactory->loadPrivateKey("./server-private-key.pem");

	sslSocketFactory->ciphers("ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");

	serverSocket = shared_ptr<TServerSocket>(new TSSLServerSocket(port, sslSocketFactory));
	// end SSL
#endif

	shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());

	shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

	TSimpleServer server(processor, serverSocket, transportFactory, protocolFactory);
	server.serve();


	return 0;
}

