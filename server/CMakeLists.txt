PROJECT(DFServer)

# define header and source files for the library
set (HEADER_FILES
	include/DistributedFlame_server.h
	include/DistributedFlameHandler.h
	include/SqliteDbManager.h
)

set (SOURCE_FILES
	src/DistributedFlame_server.cpp
	src/DistributedFlameHandler.cpp
	src/SqliteDbManager.cpp
)

# include headers
INCLUDE_DIRECTORIES(include)

INCLUDE_DIRECTORIES( ${DF_SOURCE}/common/include)
INCLUDE_DIRECTORIES(${Thrift_INCLUDE_DIR})


# setup target
ADD_EXECUTABLE(dfserver ${HEADER_FILES} ${SOURCE_FILES})

TARGET_LINK_LIBRARIES(dfserver df ${Thrift_LIBRARY} sqlite3)
