//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reoriduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------


#ifndef SQLITEDBMANAGER_H
#define SQLITEDBMANAGER_H

#include <sqlite3.h>
#include <vector>
#include <map>

//-----------------------------------------------------------------------------
class SqliteDbManager
{
	//-----------------------------------------------------------------------------
	// singleton related
	//-----------------------------------------------------------------------------
	public:
		//-----------------------------------------------------------------------------
		static SqliteDbManager* instance()
		{
			static SqliteDbManager theInstance;

			return &theInstance;
		}
		~SqliteDbManager();

		bool isValidUser(std::string user, std::string password);
		std::vector<std::map<std::string, std::string> > query(std::string query);

	//-----------------------------------------------------------------------------
	protected:
		void createTablesIfNotExist();

	//-----------------------------------------------------------------------------
	private:
		SqliteDbManager();

	//-----------------------------------------------------------------------------
	// singleton related
	//-----------------------------------------------------------------------------
	private:
		SqliteDbManager( const SqliteDbManager& op );
		SqliteDbManager& operator=( const SqliteDbManager& op );

	private:
		sqlite3 *db;
};
#endif // SQLITEDBMANAGER_H
