//-----------------------------------------------------------------------------
// Copyright (C) Jancsi A. Farkas 2011.
//
// Created in 2010 as an unpublished copyright work. All rights reserved.
//
// This document and all the information it contains is confidential and
// proprietary to Jancsi A. Farkas. Hence, it might not be used, copied,
// reproduced, transmitted or stored in any form or by any means, electronic,
// recording, photocopying, mechanical or otherwise without prior
// written permission of it's owner.
//
// Created 9/14/2011
//-----------------------------------------------------------------------------

#ifndef DISTRIBUTEDFLAMEHANDLER_H
#define DISTRIBUTEDFLAMEHANDLER_H

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "DistributedFlame.h"

//-----------------------------------------------------------------------------
class DistributedFlameHandler : virtual public DistributedFlameIf
{
	//-----------------------------------------------------------------------------
	public:
		DistributedFlameHandler();
		void authenticate(AuthenticationResult& _return, const std::string& username,
						  const std::string& password)
						  throw (InvalidUserException, apache::thrift::TException);

		void refreshAuthentication(AuthenticationResult& _return, const std::string& authToken);

		void getPublicUserInfo(PublicUserInfo& _return, const std::string& username);
		void getUser(User& _return, const std::string& authToken);

		void createJob(Job& _return, const std::string& authToken, const Job& job);
		int32_t deleteJob(const std::string& authToken, const ID jobId);
		void getJobTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId);

		void getReadyTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId);
		void getAvailableTasks(std::set<Task> & _return, const std::string& authToken, const ID jobId);
		void getNextTask(Task& _return, const std::string& authToken);
		bool UpdateTaskResult(const ID taskId, const Data& data);

	//-----------------------------------------------------------------------------
	protected:
		boost::uuids::uuid generateRandomUUID();
};

#endif // DISTRIBUTEDFLAMEHANDLER_H
